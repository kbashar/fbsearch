<?php 
    $query = $_POST['query'];
    $range = $_POST['rangeType'];
    $object = $_POST['objectType'];
    
    $URL = "https://web.facebook.com/search/str/". $query."/stories-keyword/";

    if($object === "post") {
        $rang_map_post = array(
        'live' => "intersect/stories-live" ,
        'all posts' => "intersect",
        "page post" => "stories-publishers");
        $URL .= $rang_map_post[$range];
    }
    else if ($object === "posts by date"){
        $rang_map_post = array('This week' => "this-week",
        'this month' => "this-month",
        'last week' => "last-week",
        'last month' => "last-month");
        $URL .= $rang_map_post[$range]."/date/stories/intersect";
    }
    else {
        $rang_map_pvs = array(
         'all' => "",
         'last week' => "/week-ago/after/stories",
         'last month' => "/month-ago/after/stories",
         'last year' => "/".(date("Y")-1)."/date/stories",);
        $URL .= $object."/stories".$rang_map_pvs[$range]."/intersect";
    }

    echo $URL;
?>
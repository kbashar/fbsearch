var objectType = "post";
var rangeType = "live";

window.onload = function () {
    
    itemArray=['post', 'posts by date', 'photo', 'video', 'share']
    optionArray = [
        ['Live', 'All posts', 'Page post'],
        ['This week', 'This month', 'Last week', 'Last month'],
        ['All', 'Last week', 'Last month', 'Last year'],
        ['All', 'Last week', 'Last month', 'Last year'],
        ['All', 'Last week', 'Last month', 'Last year'],]

    console.log("window loaded")

    $( ".item" ).on( "click", function( event ) {
  objectType = $(this).text().toLowerCase(); 
  console.log(objectType);
  
  var options = optionArray[itemArray.indexOf($(this).text().toLowerCase())]
  console.log(options);
  $("#list-group").empty();
  var htmls = optionRangeFactory(options);
  for(var i=0; i<htmls.length; i++) {
      $("#list-group").append(htmls[i])
  }

  $(".list-group-item").on("click", function( event ) {
      rangeType = $(this).text().toLowerCase();
      console.log(rangeType);
  })
});
$("#search").on("click", function(event) {
    console.log("query sent");
     var ajaxRequest;
    event.preventDefault();
  var query = document.getElementsByTagName("input")[0].value;
  var values = {
         objectType: objectType,
         rangeType: rangeType,
         query: query,
            }
  ajaxRequest= $.ajax({
            url: "index.php",
            type: "post",
            data: values
        });


     ajaxRequest.done(function (response, textStatus, jqXHR){
          console.log(response)
          window.location = response;
     });

     ajaxRequest.fail(function (response, textStatus, jqXHR){
         console.log(response)
     });
});
}
function optionRangeFactory(options) {
    var optionRangeHTML = [];
    for(i=0; i<options.length; i++) {
        optionRangeHTML.push('<a href="#" class="list-group-item ">'+options[i]+'</a>')
    }
    return optionRangeHTML;
}